#ifndef AIRBREAK_H
#define AIRBREAK_H

#include "loader/loader.h"
#include <SRDescent.h>
#include <SRHook.hpp>
#include <gtasa.hpp>

class AirBreak : public SRDescent {
	SRHook::Hook<> ApplyMoveSpeed{ 0x542DD0, 12 };
	SRHook::Hook<> ProcessControl{ 0x548603, 5 };
	SRHook::Hook<> UpdatePosition{ 0x5E1B15, 5 };

	bool	   status = false;
	bool	   moving = false;
	HelperInfo helper;
	float	   speed = 0.2f;
	float	   accel = 0.0f;
	RwV3D	   move	 = { 0, 0, 0 };

public:
	AirBreak( SRDescent *parent );
	virtual ~AirBreak();
};

#endif // AIRBREAK_H
