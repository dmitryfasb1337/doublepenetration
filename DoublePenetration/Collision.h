#ifndef COLLISION_H
#define COLLISION_H

#include "loader/loader.h"
#include <Patch.h>
#include <SRDescent.h>
#include <SRHook.hpp>

class Collision : public SRDescent {
	SRHook::Hook<> antiCollision{ 0x54BCF4, 6 };
	SRHook::Patch  camCars{ 0x41AB12, { 0x31, 0xC0 } };
	SRHook::Patch  camPeds{ 0x415495, { 0x31, 0xC0 } };
	SRHook::Patch  camBuilds{ 0x41A97A, { 0x31, 0xC0 } };
	SRHook::Patch  camObjs{ 0x41AC34, { 0x31, 0xC0 } };
	SRHook::Patch  camCache{ 0x41AFD6, { 0x31, 0xC0, 0x90, 0x90 } };
	uint8_t		   ab_status = 2;
	bool		   status	 = false;
	HelperInfo	   helper;

public:
	Collision( SRDescent *parent );
	virtual ~Collision();
};

#endif // COLLISION_H
